# PythonX Core | gitlab.com/z.brevnyakov/python-x

# imports



# variables

# a = print(sep='123') or 0
# b = 7 + a

# functions

def eq(element):
    return lambda t: t == element

def o(**kwargs):
    class _o(dict):
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)
      
        def __str__(self):
            return self.__dict__.__str__()
        
        def __repr__(self):
            return self.__dict__.__repr__()

        def __getitem__(self, item):
            return self.__dict__[item]
        
        def __setitem__(self, item, value):
            self.__dict__[item] = value

    return _o(**kwargs)

def p(*args, **kwargs):
    from sys import stdout
    ostream = kwargs['stream'] if 'stream' in kwargs else stdout
    sep = kwargs['sep'] if 'sep' in kwargs else ' '
    end = kwargs['end'] if 'end' in kwargs else '\n'
    ostream.write(sep.join([str(_x) for _x in args]) + end)

def inp(*args, **kwargs): #TODO: ADD SPLIT=BOOL KWARG, ADD REPEAT=INT KWARG
    from sys import stdin 
    def v(e, t):
        return type(e) == t

    istream = kwargs['stream'] if 'stream' in kwargs else stdin
    u_inp = istream.readline()
    
    if (u_inp[-1] == '\n'):
        u_inp = u_inp[:-1]
    if (len(args) == 0):
        return u_inp
    else:
        a = args[0]

        if len(args) == 1 and (v(a, bool) and a or v(a, int) and a == 1):
            return px(u_inp.split())
        elif v(a, str):
            return px(u_inp.split(a))
        elif callable(a):
            return px([a(x) for x in u_inp.split()])
        elif v(a, int) and a >= 1:
            if (len(args) == 1):
                return px([u_inp] + [input() for x in range(args[0] - 1)])
            else:
                res = [args[1](x) for x in u_inp.split()]
                for _x in range(a - 1):
                    res.append(*inp(args[1]))
                return px(res)

# classes

class px:
    def __iter__(self):
        return iter(self.iterator)
    
    def __next__(self):
        return next(self.iterator)

    def __init__(self, *args, **kwargs):
        def is_iterable(e):
            ii = True
            try:
                e = iter(e)
            except:
                ii = False
            return ii

        args_count = len(args)

        if args_count == 0:
            self.iterator = iter([])
        elif args_count == 1:
            [first_arg] = args
            first_arg_type = type(first_arg)

            if first_arg_type == px or is_iterable(first_arg):
                self.iterator = iter(first_arg)
            elif first_arg_type == int:
                # 123 -> (1, 2, 3)
                self.iterator = map(int, str(first_arg))
            elif first_arg_type == float:
                # 123.01 -> ((1, 2, 3), (0, 1))
                self.iterator = map(lambda part: map(int, part), str(first_arg).split('.'))
            else:
                self.iterator = iter((first_arg,))
        else:
            self.iterator = iter(args)

    def xsplit(self, condition=lambda el: True):
        from itertools import groupby
        return px(map(lambda res: px(res[1]),
                filter(lambda res: res[0],
                    groupby(self, lambda el: not condition(el)))))

    def map(self, func):
        return px(map(func, self))
    
    def aggregate(self, func, starting_value):
        from itertools import accumulate
        return accumulate(self, func=func, initial=starting_value)

    def to(self, new_type, join_to_string=False):
        return new_type(self) if not(join_to_string) else (
            new_type(''.join(list(map(lambda x: str(x), self.iterator))))
        )
    
    def slice(self, _from, _to, _step=1):
        from itertools import islice
        return px(islice(self, _from, _to, _step))

    def sort(self, key=None, reverse=False):
        return px(sorted(self, key=key, reverse=reverse))

    def print_as_list(self, start='', end='\n'):
        #TODO: FIX MEMORY ISSUES (?) BY EL-AFTER-EL printing
        p(start + '[', end='')
        p(*self, sep=', ', end=']' + end)
        return self
    
    def join_and_print(self, joining_str=' ', **kwargs):
        p(joining_str.join([str(x) for x in self]), **kwargs)

    def print_all(self, **kwargs):
        p(*self, **kwargs)
        return self

    def filter(self, func):
        return px(filter(func, self.iterator))

    def call(self, func):
        return func(self)

    def safe_call(self, func):
        func(self)
        return self

    def apply(self, func):
        return px(func(self))

    def run(self):
        for _ in self.iterator:
            pass

        return self

    #def xfilter(self, cond):
    #    result = []
    #    for x in range(len(self.iterator)):
    #        if (cond(self, x)):
    #            result.append(self[x])
    #    return px(result)

    def chain(self, another_iterator):
        from itertools import chain
        return px(chain(self, another_iterator))
