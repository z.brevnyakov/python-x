import ast
from queue import Queue
from typing import Callable, Dict, List
from xtender_api.preprocessing_types import ArrowFunctionDefinition, CurrentStatementInClosestContainer, UnprocessableCodePartsState
from xtender_api.logging import error



def validate_preprocess_operation(
            unprocessable_code_parts: UnprocessableCodePartsState,
            code_part_starting_index: int,
            left_shift=0):
    if (not unprocessable_code_parts.has_unprocessable_code_parts
            or code_part_starting_index + left_shift <= unprocessable_code_parts.left_bound
            or code_part_starting_index + left_shift >= unprocessable_code_parts.right_bound):
        return True
    
    for (ix_start, ix_end) in unprocessable_code_parts.parts:
        # Found a match: we're inside some unprocessable region
        if (ix_start <= code_part_starting_index + left_shift <= ix_end):
            return False

        # Since ucp are always sorted, this condition means the index is between two regions
        # so is safe
        elif (code_part_starting_index + left_shift < ix_start):
            return True

        # Else it's higher that both start and end so we can go to next iteration
    
    # Means index is outside of all regions = safe
    return True


def arrow_function_name_generator():
    i = 0
    while True:
        yield f'_pythonx_f{ i }'
        i += 1


def get_opposite_char(target_char):
    opposite_box_map = {
        '{': '}',
        '[': ']',
        '(': ')',
        
        '}': '{',
        ']': '[',
        ')': '('
    }
    
    return opposite_box_map[target_char]
   

def unsafe_readbox(base_string: str, target_char: str, starting_index: int, reverse=False):
    '''Unsafe: ignores chars escaping, does not skip unprocessable code parts (string literals and comments), does not analyze nested boxes.
    Runs find/rfind for the char opposite to `target_char`.'''
    
    if reverse:
        return base_string.rfind(get_opposite_char(target_char), starting_index - 1)
    
    return base_string.find(get_opposite_char(target_char), starting_index)


def unsafe_readword(base_string: str, starting_index: int, reverse=False):
    '''
    Reads one word composed of characters matching condition `char.isalpha() or char.isdigit() or char == '_'`, returns it's end. 
    - Unsafe: does not consider unprocessible code parts.
    '''
    
    condition = lambda char: char.isalpha() or char.isdigit() or char == '_'
    
    direction_int = -1 if reverse else 1

    current_index = starting_index
    while 0 <= current_index < len(base_string):
        if not condition(base_string[current_index]):
            return current_index - direction_int
        
        current_index += direction_int
    else:
        if not condition(base_string[current_index]):
            return current_index - direction_int
        return current_index


def generic_unsafe_readbox(base_string: str, target_char_condition: Callable[[str], bool], starting_index: int, reverse=False):
    '''Unsafe: ignores chars escaping, does not skip unprocessable code parts (string literals and comments), does not analyze nested boxes.
    Finds first character matching `target_char_condition` and returns its index.'''
    
    direction_int = -1 if reverse else 1

    current_index = starting_index
    while 0 <= current_index < len(base_string):
        if target_char_condition(base_string[current_index]):
            return current_index
        
        current_index += direction_int
    else:
        error('Generic unsafe readbox failed.')
        exit(1)


def readbox(base_string: str, target_char: str, starting_index: int, unprocessable_code_parts: UnprocessableCodePartsState, reverse=False):
    '''Safe: skips unprocessable code parts (string literals and comments)'''
    
    direction_int = -1 if reverse else 1
    # such decider causes repetative searches. E.g. for string: "{_{_{_{_}" there would be 4*2=8 searches instead of 5.
    # in realistic scenarios for this project (python-x), this should not cause performance issues.
    current_index_decider = max if reverse else min
    target_char_opposite = get_opposite_char(target_char)
    
    def finding_func(__sub, __start):
        ix = __start + 1 if not reverse else __start
        find_func = base_string.find if not reverse else lambda __sub, ix: base_string.rfind(__sub, 0, ix)

        while ix != -1:
            ix = find_func(__sub, ix)
            if validate_preprocess_operation(unprocessable_code_parts, ix):
                return ix
            ix += direction_int
            
        return -1
        
    current_index = starting_index
    
    next_target_char_index = finding_func(target_char, current_index)
    next_opposite_char_index = finding_func(target_char_opposite, current_index)

    # there are no nested boxes of this type
    # TODO: -1 & -1 check required?
    if (not reverse and next_opposite_char_index < next_target_char_index
            or reverse and next_target_char_index < next_opposite_char_index):
        return next_opposite_char_index
    
    # if there are:
    current_index = starting_index
    # if box_char_counter is zero, second box border is found
    # if it is becomes higher than 1, we found a nested box and need 2 (or more) unescaped closing box characters
    # if it becomes lower than zero, we found a box like ") ... (" which might be bad, but we won't bother if counter becomes zero in the end.
    box_char_counter = 1
    success = False
    res_index = -1
    while 0 <= current_index < len(base_string):
        # this might cause up to 1 extra substring search per one readbox scan
        # which is not good, but allows not to search via this `while` loop
        # but by python's find/rfind.
        # in realistic scenarios for this project (python-x) this should not cause performance issues.
        #
        # `re` module cannot be used because it cannot parse right-to-left.
        # `regex` pip module could be an alternative, but right now this project does
        # not have any dependencies of that kind and it seems to be unchangeable in foreseeable future.
        next_target_char_index = finding_func(target_char, current_index)
        next_opposite_char_index = finding_func(target_char_opposite, current_index)
        
        if next_target_char_index == -1 and next_opposite_char_index == -1:
            break
        elif next_target_char_index == -1:
            box_char_counter -= 1
            res_index = next_opposite_char_index
            
            while (0 <= res_index < len(base_string)) and box_char_counter > 0:
                res_index = finding_func(target_char_opposite, res_index)
                box_char_counter -= 1
            
            break
        elif next_opposite_char_index == -1:
            box_char_counter += 1
            res_index = next_target_char_index
            
            while (0 <= res_index < len(base_string)) and box_char_counter < 0:
                res_index = finding_func(target_char, res_index)
                box_char_counter += 1
            
            break

        # `direction_int` is used to handle these conditions depending on `reverse` parameter
        if next_target_char_index > next_opposite_char_index:
            box_char_counter -= direction_int
        else:
            box_char_counter += direction_int
            
        if box_char_counter == 0:
            break
        
        res_index = current_index = current_index_decider(next_target_char_index, next_opposite_char_index)
    
    success = (box_char_counter == 0)
    
    if not success:
        error(f'No enclosing box character found. Character: "{target_char}"; final character difference: {box_char_counter}')
        exit(1)
        
    return res_index


def find_current_statement_in_closest_statement_container(node, af_name) -> CurrentStatementInClosestContainer:
    if not (hasattr(node, 'parent')):
        error(f'Critical error while pasting arrow function definition (af_name="{af_name}"): Name node has NO parent!')
        raise RuntimeError(f'Can\'t find parent statement for "{af_name}" arrow function!')
    
    current_parent = prev_parent = node

    good_parent_node_condition = lambda: hasattr(current_parent, 'body') and isinstance(current_parent.body, list)
    while hasattr(current_parent, 'parent') and not good_parent_node_condition():
        prev_parent = current_parent
        current_parent = current_parent.parent

    if not hasattr(current_parent, 'body'):
        error('Critical error while pasting arrow function definition (af_name="{af_name}"):'
              'current statement and its parent could not be found!')
        raise RuntimeError(f'Can\'t find parent statement for "{af_name}" arrow function!')

    return CurrentStatementInClosestContainer(
        current_statement_container=current_parent,
        current_statement_node=prev_parent)


def parse_af_def(af_def: ArrowFunctionDefinition):
    name = af_def.name
    args = af_def.args_str
    body = af_def.body_str

    definition_src = \
        f'def {name}({args}):\n{body}'
    
    return ast.parse(definition_src)


def paste_af_definition(current_statement, statement_container, af_ast: ast.AST):
    ix = statement_container.body.index(current_statement)
    statement_container.body.insert(ix, af_ast)

    return statement_container


def find_original_af_defs_nodes(general_ast: ast.AST, af_defs: List[ArrowFunctionDefinition]):
    af_defs_names = [ af_def.name for af_def in af_defs ]

    class Visitor:
        ...


def build_af_ast(af_def: ArrowFunctionDefinition, ast_af_def_cache: Dict[ArrowFunctionDefinition, ast.AST]):
    af_to_ast_queue: Queue[ArrowFunctionDefinition] = Queue()
    [ af_to_ast_queue.put(i) for i in af_def.contains_definitions ]

    while not af_to_ast_queue.empty():
        current_func = af_to_ast_queue.get()

        not_built_af_defs = list(filter(lambda af_def: af_def not in ast_af_def_cache,
                                        current_func.contains_definitions))
        
        if len(not_built_af_defs):
            af_to_ast_queue.put(current_func)
            [ af_to_ast_queue.put(i) for i in not_built_af_defs ]
            continue
        
        af_ast = parse_af_def(af_def)

        af_original_nodes: Dict[ArrowFunctionDefinition, ast.AST] = ...

        for af_inner_def in current_func.contains_definitions:
            inner_def_ast = ast_af_def_cache[af_inner_def]

            # ast.Name
            inner_def_original_node = af_original_nodes[af_inner_def]

            inner_def_statement_data = \
                find_current_statement_in_closest_statement_container(
                    inner_def_original_node, af_inner_def.name)
            


