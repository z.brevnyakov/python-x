import io
import sys
import logging
from enum import Enum
from datetime import datetime
from rich import pretty
from rich.logging import RichHandler
from rich.syntax import Syntax
from rich.console import Console
from rich.panel import Panel
import os


class LOGGER_MODE(Enum):
    VERBOSE = 4 # all logs
    DEFAULT = 1 # no general logs
    SILENT = 2 # only errors


show_debug_messages = False
console = Console()
logs = logging.getLogger('rich')
initialized = False

pretty.install()


def init(level: LOGGER_MODE = LOGGER_MODE.DEFAULT):
    global logs, initialized, show_debug_messages
    
    if level == LOGGER_MODE.VERBOSE:
        show_debug_messages = True
    
    FORMAT = '[PXX] %(message)s'

    logging.basicConfig(
        level=level.value, format=FORMAT, datefmt="[%X]", handlers=[RichHandler(rich_tracebacks=True)]
    )
    
    logs.setLevel(level.value)
    
    initialized = True


def after_init_only(func):
    def wrapper(*args, **kwargs):    
        if not initialized:
            raise RuntimeError('Logger not initialized')
        return func(*args, **kwargs)
    return wrapper


@after_init_only
def log(*args):
    logs.log(LOGGER_MODE.VERBOSE.value, ' '.join(map(str, args)))


@after_init_only
def error(*args):
    logs.exception(f'[{datetime.now()}] ERROR:' + ' '.join(map(str, args)))


@after_init_only
def warn(*args):
    logs.log(LOGGER_MODE.DEFAULT.value, f'WARNING:' + ' '.join(map(str, args)))


@after_init_only
def log_python_code(code_piece):
    syntax = Syntax(code_piece, "python")
    
    print()
    console.print(syntax)
    print()


@after_init_only
def print_highlighted_string_regions(string, regions_list, title=None, is_debug_message=True):
    if is_debug_message and not(show_debug_messages):
        return

    last_region_right_border = -1
    
    output = ''
    
    for left_border, right_border in regions_list:
        output += string[last_region_right_border + 1:left_border]
        log(['FOR_PRINT', left_border, string[left_border]])
        output += '[on #005050]' + string[left_border:right_border + 1] + '[/]'
        last_region_right_border = right_border
    
    output += string[last_region_right_border + 1:]
    
    console.print(Panel(output, title=title))
