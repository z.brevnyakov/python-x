from typing import Dict, List, Tuple
from xtender_api.logging import error, log, print_highlighted_string_regions
from xtender_api.str_utils import find_comments_and_string_literal_borders
from itertools import chain



class UnprocessableCodePartsState:
    def __init__(self, parts_data: List[Tuple[int,int]]):
        self.has_unprocessable_code_parts = False
        self.left_bound = -1
        self.right_bound = -1
        self.parts: List[Tuple[int,int]] = parts_data

        if len(self.parts) == 0:
            return

        first_piece, last_piece = self.parts[0], self.parts[-1] 
        
        self.has_unprocessable_code_parts = True
        self.left_bound = first_piece[0]
        self.right_bound = last_piece[1]
    
    
    @staticmethod
    def from_source(source: str):
        unprocessable_code_parts_data = find_comments_and_string_literal_borders(source)
        return UnprocessableCodePartsState(unprocessable_code_parts_data)


    def shift_parts(self, diff: int, ix_from=None, ix_to=None):
        if ix_from == None:
            ix_from = 0
        if ix_to == None:
            ix_to = len(self.parts)

        for ix in range(ix_from, ix_to):
            left, right = self.parts[ix]
            self.parts[ix] = (left + diff, right + diff)
        
        self.recalculate_cached_properties()


    def recalculate_cached_properties(self):
        self.has_unprocessable_code_parts = bool(len(self.parts))
        
        if not self.has_unprocessable_code_parts:
            self.left_bound = -1
            self.right_bound = -1
            
        first_piece, last_piece = self.parts[0], self.parts[-1] 
        
        self.left_bound = first_piece[0]
        self.right_bound = last_piece[1]


    def is_within_bounds(self, search_index):
        return self.left_bound <= search_index <= self.right_bound
        

    def get_closest_regions(self, left_border: int, right_border: int) -> Tuple[int, int]:
        """
        Returns two indices of regions:
        1) closest region to the left of `left_border`
        2) closest region to the right of `right_border`
        
        If border intersects a region, the resulting index is the index of the intersected region.
        """
        # TODO tests
        
        if not self.has_unprocessable_code_parts:
            return (-1, -1)

        left_region_ix = 0
        right_region_ix = len(self.parts) - 1

        while left_region_ix != len(self.parts) - 1 and self.parts[left_region_ix][0] < left_border:
            left_region_ix += 1
            
            if self.parts[left_region_ix][0] > left_region_ix:
                left_region_ix -= 1
                break

        while right_region_ix != 0 and self.parts[right_region_ix][1] > right_border:
            right_region_ix -= 1
            
            if self.parts[right_region_ix][1] < right_border:
                right_region_ix += 1
                break

        if left_region_ix == len(self.parts) and right_region_ix == -1:
            # This, however, does not cover the situation when right is not the last but left was not found
            # (which must also be impossible)
            raise RuntimeError('(Impossible) No left or right unprocessable code regions found.')

        # when all regions are to the left of both indices
        # if left_region_ix == len(self.parts):
        #     return (-1, -1)

        # when all regions are to the right of both indices
        # if right_region_ix == -1:
        #     return (-1, -1)
        
        return (left_region_ix, right_region_ix)
        

    def get_unprocessable_code_parts_within_borders(self, left_border: int, right_border: int):
        left_region_ix, right_region_ix = self.get_closest_regions(left_border, right_border)
        
        log(['bounds extraction', left_region_ix, right_region_ix])
        
        if -1 == left_region_ix == right_region_ix:
            return []
        
        # L R [ ] ...
        # ... [ ] L R
        # ... [ L R ] ...
        if left_region_ix == right_region_ix:
            log(['bounds extr: one region'])
            region_ix = left_region_ix
            region_left_border, region_right_border = self.parts[region_ix]
            
            log(['request', (left_border, right_border)])
            log(['region', self.parts[region_ix]])
            
            if right_border < region_left_border or left_border > region_right_border:
                return []
            
            return [ (max(left_border, region_left_border), min(right_border, region_right_border)) ]
        
        left_region_left_border, left_region_right_border = self.parts[left_region_ix]
        right_region_left_border, right_region_right_border = self.parts[right_region_ix]

        # ... [  ] L [  ] ... [  ] R [  ] ...
        # ... [  ] L [  ] ... [  ] [ R ] [  ] ...
        # ... [  ] [ L ] [  ] ... [  ] R [  ] ...
        # ... [  ] [ L ] [  ] ... [  ] [ R ] [  ] ...
        
        if left_border > left_region_right_border and right_border < left_region_left_border:
            return []
        
        # Within left region or not, within right region or not
        left_region_final_left_border = max(left_border, left_region_left_border)        
        right_region_final_right_border = min(right_border, right_region_right_border)
        
        cut_left_region = (left_region_final_left_border, left_region_right_border)
        cut_right_region = (right_region_left_border, right_region_final_right_border)
        
        return [ cut_left_region ] + self.parts[left_region_ix + 1 : right_region_ix] + [ cut_right_region ]


    def recalculate_unprocessable_code_parts_on_code_change(
            self,
            modified_region_left_border: int,
            modified_region_old_right_border: int,
            modified_region_new_right_border: int,
            ucps_for_replacement: None | List[Tuple[int, int]] = None):
        """
        - All UCPs to the right of `modified_region_old_right_border` get their border indices decreased by
        `diff = modified_region_old_right_border - modified_region_new_right_border`.
        - UCPs included inside of `(modified_region_left_border, modified_region_old_right_border)` are removed
        - Partially intersected UCPs are cut
        - If ucps_for_replacement is provided, those are not removed/cut but rather replaced.
        
        TODO process intersections appearing due to replacements.
        TODO add other ucp properties recalculating
        """
        
        # TODO add tests to check if ucp are sorted
        if ucps_for_replacement == None:
            ucps_for_replacement = []
        
        # Had nothing, added nothing
        if not self.has_unprocessable_code_parts and len(ucps_for_replacement) == 0:
            return

        last_region_right_border = self.parts[-1][1]
        
        # Added nothing, source changed to right all regions which means nothing must be shifted
        if last_region_right_border < modified_region_left_border and len(ucps_for_replacement) == 0:
            return
        
        diff = modified_region_new_right_border - modified_region_old_right_border
        if diff > 0:
            diff -= 1

        left_region_ix, right_region_ix = \
            self.get_closest_regions(modified_region_left_border, modified_region_old_right_border)
        
        log([ 'Closest regions', left_region_ix, right_region_ix ])
        
        if left_region_ix == right_region_ix:
            log('Single region')
            region_ix = left_region_ix
            region_left_border, region_right_border = self.parts[region_ix]
            
            if modified_region_old_right_border < region_left_border:
                log('Single region with no intersection')
                log(['diff', diff])
                self.shift_parts(diff, region_ix)
                return
            
            split_region_left = (region_left_border, modified_region_old_right_border)
            split_region_right = (modified_region_new_right_border, region_right_border + diff)
            
            log('split_region_left', split_region_left)
            log('split_region_right', split_region_right)
            
            self.parts = self.parts[:region_ix] + [split_region_left] + ucps_for_replacement + [split_region_right] \
                + [ (left + diff, right + diff) for left, right in self.parts[region_ix + 1:]] 
            return
            
        left_region = self.parts[left_region_ix]
        right_region = self.parts[right_region_ix]
        
        has_left_region_intersection = left_region[0] <= modified_region_left_border <= left_region[1]
        has_right_region_intersection = right_region[0] <= modified_region_old_right_border <= right_region[1]
        
        log(['L, R intersections', has_left_region_intersection, has_right_region_intersection])
        
        if has_left_region_intersection:
            self.parts[left_region_ix] = (left_region[0], modified_region_left_border)
            left_region_ix += 1 # cut region must not be deleted
        
        if has_right_region_intersection:
            self.parts[right_region_ix] = (modified_region_old_right_border, right_region[1])
            log(['updated RR', self.parts[right_region_ix]])
            right_region_ix -= 1
        
        log(['diff', diff])
        
        updated_right_regions = [ (left_border + diff, right_border + diff)
                                 for left_border, right_border in self.parts[right_region_ix:]]

        self.parts = [ *self.parts[:left_region_ix + 1], *ucps_for_replacement, *updated_right_regions ]
        
        self.recalculate_cached_properties()
        
        # TODO process intersections appearing due to replacements (error if those occur)
        # TODO possibly, unite regions if next starts right after previous ends
        

class ArrowFunctionDefinition:
    def __init__(self, func_name: str, args_str: str, body_str: str, ucp_parts: List[Tuple[int, int]]):
        self.contains_definitions: list[ArrowFunctionDefinition] = [] # (Generated) names of arrow functions to be defined inside 
        self.ucp: UnprocessableCodePartsState = UnprocessableCodePartsState(ucp_parts)
        self.name = func_name
        self.args_str = args_str
        self.body_str = body_str


class PreprocessorState:
    def __init__(self, source_code):
        self.let_expressions_detected = False
        self.arrow_functions_detected = False
        self.long_body_arrow_functions_detected = False
        self.arrow_functions_definitions: Dict[str, ArrowFunctionDefinition] = {}
        self.top_level_contains_definitions_of: list[ArrowFunctionDefinition] = []
        
        self.unprocessable_code_parts = UnprocessableCodePartsState.from_source(source_code)
        print_highlighted_string_regions(source_code, self.unprocessable_code_parts.parts, title='Original UCP')


class CurrentStatementInClosestContainer():
    def __init__(self, current_statement_node, current_statement_container):
        self.current_statement_node = current_statement_node
        self.current_statement_container = current_statement_container