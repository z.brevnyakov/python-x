from enum import Enum
from typing import Callable, List, Tuple
from xtender_api.logging import error, log, warn, print_highlighted_string_regions



SPACING_CHARS = ' \n\r\t'
STRING_LITERAL_BORDER_CHARS = ["'", '"']
FSTRING_TOKEN_STARTING_CHARS = STRING_LITERAL_BORDER_CHARS + ['{', '}']


class FstringToken(Enum):
    SINGLE_QUOTE = 0
    DOUBLE_QUOTE = 1
    TRIPLE_SINGLE_QUOTE = 2
    TRIPLE_DOUBLE_QUOTE = 3
    OPEN_BRACE = 4
    CLOSING_BRACE = 5 # Should not appear in stack


class FstringReadingState(Enum):
    CONSTANT_REGION = 0
    EXPRESSION_REGION = 1
    

class FstringConstantSubregionMode(Enum):
    FSTRING = 0
    DEFAULT = 1



def expect(substring, source_string, from_index, reverse: bool = False) -> bool:
    '''
        `substring` - expected substring
        `source_string` - string to look into
        `from_index` - index from which to look
        `direction` - direction of checking (`< 0` for left, `> 0` for right which is set by default)
    '''
    
    substring_len = len(substring)
    
    if reverse:
        return source_string[from_index - substring_len + 1:from_index + 1] == substring
    else:
        return source_string[from_index:from_index + substring_len] == substring


def read_fstring_token(source_string: str, char_index: int) -> Tuple[FstringToken, int]:
    char = source_string[char_index]
    is_triple_quoted = expect(char * 3, source_string, char_index)
    
    token = FstringToken.SINGLE_QUOTE
    after_token_index = -1
    
    if is_triple_quoted:
        token = {
            "'": FstringToken.TRIPLE_SINGLE_QUOTE,
            '"': FstringToken.TRIPLE_DOUBLE_QUOTE
        }[char]
        
        after_token_index = char_index + 3
    else:
        token = {
            "'": FstringToken.SINGLE_QUOTE,
            '"': FstringToken.DOUBLE_QUOTE,
            '{': FstringToken.OPEN_BRACE,
            '}': FstringToken.CLOSING_BRACE
        }[char]
        
        after_token_index = char_index + 1
        
    return (token, after_token_index)


def opposite_fstring_token(token: FstringToken):
    braces_case = {
        FstringToken.OPEN_BRACE: FstringToken.CLOSING_BRACE,
        FstringToken.CLOSING_BRACE: FstringToken.OPEN_BRACE
    }
    
    return braces_case[token] if (token in braces_case) else token


def is_quote_token(token: FstringToken):
    if token not in [ FstringToken.OPEN_BRACE, FstringToken.CLOSING_BRACE ]:
        return True


def get_fstring_constant_regions(source_string: str, fstring_first_quote_index: int):#TODO add left region index saving
    """
    if code is: `f"a{123}b"`, result would be `[(0, 3), (7, 9)]`
    """
    # TODO: comments inside fstrings?
    constant_regions = []
    current_region_left_index = fstring_first_quote_index - 1
    current_region_right_index = -1
    reading_state: FstringReadingState = FstringReadingState.CONSTANT_REGION
    constant_subregion_mode: FstringConstantSubregionMode = FstringConstantSubregionMode.FSTRING
    
    first_token, current_index = read_fstring_token(source_string, fstring_first_quote_index)

    tokens_stack: List[Tuple[int, FstringToken]] = [ (current_region_left_index, first_token) ]

    while current_index < len(source_string) and tokens_stack:
        next_char = ''
        
        current_char = source_string[current_index]
        if current_index + 1 < len(source_string):
            next_char = source_string[current_index + 1]
        
        if current_char == '\\':
            current_index += 2
            continue
        
        if reading_state == FstringReadingState.CONSTANT_REGION and current_char + next_char in [ '{{', '}}' ]:
            current_index += 2
            continue
        
        if not current_char in FSTRING_TOKEN_STARTING_CHARS:
            current_index += 1
            continue
        
        token_first_index = current_index
        token, current_index = read_fstring_token(source_string, current_index)
        
        # We start in a constant region, with "constant_subregion_mode" set to FSTRING
        # We may then find the end of the literal (if no [more] expressions in braces found)
        # or find a brace, which means entering an expression subregion.
        # In expression subregion, we remember braces in stack, and after their count reach zero,
        # assume this subregion is over and outer constant (sub-)region is continued;
        # or, if we find a quote, we enter a new subregion.
        if reading_state == FstringReadingState.CONSTANT_REGION:
            # In constant & fstring constant region we are waiting for:
            # - Enclosing quote(s) token
            # - Open brace (to enter a new expression subregion)
            if constant_subregion_mode == FstringConstantSubregionMode.FSTRING:
                # As of Python 3.11, you cannot have nested fstring with
                # the same border quotes as one outer has.
                # Which means we can break the loop - there's either
                # an end of the literal or a syntax error.
                if token == first_token: 
                    current_region_right_index = current_index - 1
                    constant_regions.append((current_region_left_index, current_region_right_index))
                    
                    tokens_stack.pop()
                    # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')
                    break
                elif token == FstringToken.OPEN_BRACE:
                    current_region_right_index = current_index - 1
                    constant_regions.append((current_region_left_index, current_region_right_index))
                    
                    tokens_stack.append((current_region_right_index, token))
                    # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')
                    reading_state = FstringReadingState.EXPRESSION_REGION
                elif token == FstringToken.CLOSING_BRACE:
                    # do nothing because of a syntax error
                    warn('Found an unescaped closing brace when parsing fstring constant regions.',
                        f'(starting index: {token_first_index}, index after token: {current_index})')
                # a quote token
                else:
                    if opposite_fstring_token(token) == tokens_stack[-1][1]:
                        current_region_right_index = current_index - 1 # TODO used to be without "- 1", can't remember why
                        constant_regions.append((current_region_left_index, current_region_right_index))
                        
                        reading_state = FstringReadingState.EXPRESSION_REGION
                        tokens_stack.pop()
                        # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, constant_regions, title='UCP (intermediate state)')
            # In constant & default subregion we are waiting for quotes ONLY,
            # so we could return to outer expression region
            else:
                # If true, we found the end of the nested literal
                # Braces are ignored
                # Stack emptyness check not required
                if is_quote_token(token) and opposite_fstring_token(token) == tokens_stack[-1][1]:
                    current_region_right_index = current_index - 1
                    constant_regions.append((current_region_left_index, current_region_right_index))
                    
                    reading_state = FstringReadingState.EXPRESSION_REGION
                    tokens_stack.pop()
                    # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')
        # In expression region, we're waiting for:
        # - New quotes - to enter subregion
        # - Closing brace (to return to constant region/subregion)
        else:
            if token == FstringToken.CLOSING_BRACE:
                last_token_from_cache = tokens_stack[-1][1]
                if last_token_from_cache == FstringToken.OPEN_BRACE:
                    current_region_left_index = current_index - 1
                    tokens_stack.pop()                      
                    # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')
                    
                    reading_state = FstringReadingState.CONSTANT_REGION
                    constant_subregion_mode = FstringConstantSubregionMode.FSTRING
                else:
                    error('Found an extra closing brace when parsing fstring constant regions.',
                        f'(starting index: {token_first_index}, index after token: {current_index})')
                    exit(1)
            # Dict, set or PythonX arrow function
            elif token == FstringToken.OPEN_BRACE:
                tokens_stack.append((current_index, token))
                # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')
            # This means we found a quote token.
            else:
                # Nested fstring
                if source_string[token_first_index - 1].lower() == 'f':
                    reading_state = FstringReadingState.CONSTANT_REGION
                    constant_subregion_mode = FstringConstantSubregionMode.FSTRING
                    
                    current_region_left_index = token_first_index - 1
                    tokens_stack.append((current_region_left_index, token))
                    # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')
                # Nested string
                else:
                    reading_state = FstringReadingState.CONSTANT_REGION
                    constant_subregion_mode = FstringConstantSubregionMode.DEFAULT
                    current_region_left_index = token_first_index
                    tokens_stack.append((current_region_left_index, token))
                    # print_highlighted_string_regions(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')(source_string, map(lambda pair: (pair[0], pair[0]), tokens_stack), title='UCP tokens stack (intermediate state)')

    if len(tokens_stack) == 0:
        current_region_right_index = current_index
        
        return constant_regions
    else:
        error('Error while parsing fstring constant regions: stack is not empty after parsing. Stack data:', [*map(FstringToken, tokens_stack)])
        exit(1)


def find_string_literal_second_border(source_string: str, starting_index):
    string_len = len(source_string)
    current_index = starting_index

    border_char = source_string[current_index]

    # Preliminary check
    if border_char not in STRING_LITERAL_BORDER_CHARS:
        error(
            'Tried to parse a string literal but source string does not have a string literal border character here.',
            '\nIndex:', current_index,
            '\nFound character:', f'"{border_char}"')
    
    is_a_multiline_string_literal = expect(border_char * 3, source_string, current_index)
    current_index += 1

    # skip two other quotes
    if is_a_multiline_string_literal:
        current_index += 2
    
    while (current_index < string_len):
        # skip escaped chars
        while (source_string[current_index] == '\\' and (current_index < string_len)):
            current_index += 2
        
        char = source_string[current_index]
        
        if char == border_char:
            if is_a_multiline_string_literal:
                if expect(border_char * 3, source_string, current_index):
                    return current_index + 2 # +2 because need index of ''('), not (')''
            else:
                return current_index
        
        current_index += 1
    
    error('Could not find string literal second border (reached end of source string)')
    exit(0)


def find_comment_second_border(source_string: str, starting_index: int):
    border_char = source_string[starting_index]
    
    # preliminary check
    if border_char != '#':
        error(
            'Tried to parse a comment but source string does not have a # character here.',
            '\nIndex:', starting_index,
            '\nFound character:', f'"{border_char}"')
        exit(0)

    ix_end = source_string.find('\n', starting_index)
    
    if ix_end == -1:
        return len(source_string) - 1
    
    return ix_end - 1 # -1 to not include "\n"


def find_comments_and_string_literal_borders(source_string: str):
    '''Returns `[(ix_start, ix_end), (ix_start, ix_end), ...]`'''

    result: List[Tuple[int, int]] = []
    string_len = len(source_string)
    current_index = 0
    
    while (current_index < string_len):
        char = source_string[current_index]
        
        if char == '#':
            ix_end = find_comment_second_border(source_string, current_index)
            result.append((current_index, ix_end))
            current_index = ix_end
        elif char in ['"', "'"]:
            if expect('f', source_string, current_index - 1, reverse=True):
                fstring_constant_regions = get_fstring_constant_regions(source_string, current_index)
                result += fstring_constant_regions
                current_index = fstring_constant_regions[-1][-1]
            else:
                ix_end = find_string_literal_second_border(source_string, current_index)
                result.append((current_index, ix_end))
                current_index = ix_end

        current_index += 1
    return result


def expect_after_spacing(base_string: str, target_string: str, starting_index: int, reverse=False):
    '''
        if in string `"let \\n {}"` we call it after `let` to expect `{`,
        it should return True, and False when called to expect `}`.
    '''
    
    direction_int = -1 if reverse else 1

    current_index = starting_index
    while (-1 < current_index < len(base_string)) and base_string[current_index] in SPACING_CHARS:
        current_index += direction_int
    
    if current_index in [-1, len(base_string)]:
        return False

    return expect(target_string, base_string, current_index, reverse=reverse)


def generic_expect_after_spacing(base_string: str, char_test_function: Callable[[str], bool], starting_index, reverse=False):
    step = -1 if reverse else 1
    
    current_index = starting_index
    while (-1 < current_index < len(base_string)) and (base_string[current_index] in SPACING_CHARS):
        current_index += step

    if current_index in [-1, len(base_string)]:
        return False

    return char_test_function(base_string[current_index])


def skip_spacing(base_string, starting_index, reverse=False):
    '''
    Returns the index after whitespaces (direction is set by `reverse` flag).
    Return -1 if reached one of base_string ends. 
    '''
    
    step = -1 if reverse else 1
    
    current_index = starting_index
    while (-1 < current_index < len(base_string)) and (base_string[current_index] in SPACING_CHARS):
        current_index += step
    
    if current_index in [-1, len(base_string)]:
        return -1
    
    return current_index
