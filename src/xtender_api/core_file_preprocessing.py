from ast import Assign, ClassDef, FunctionDef, Module
from typing import Literal
from xtender_api.core_library_types import CoreLibrary, Definition, IntrafileDependecy
from xtender_api.logging import error, log

from xtender_api.ast_utils import KnownDependenciesCollector, ast_dump, ast_from_file, find_known_dependencies_in_ast, pp_ast



def load_core_ast(path_to_core_file: str):
    log('Loading core ast')
    return ast_from_file(path_to_core_file)


def parse_core(core_ast: Module):
    log('Parsing core')
    top_level_definitions = core_ast.body
    
    detected_functions: dict[str, IntrafileDependecy] = {}
    detected_classes: dict[str, IntrafileDependecy] = {}
    detected_variables: dict[str, IntrafileDependecy] = {}
    
    for definition in top_level_definitions:
        if isinstance(definition, FunctionDef):
            detected_functions.update({ definition.name: IntrafileDependecy('function', definition.name) })
        elif isinstance(definition, ClassDef):
            detected_classes.update({ definition.name: IntrafileDependecy('class', definition.name) })
        elif isinstance(definition, Assign):
            # TODO: REWRITE FOR CORRECT MULTIASSIGNMENT HANDLING
            for assign_target in definition.targets:
                detected_variables.update({ getattr(assign_target, 'id'): IntrafileDependecy('variable', getattr(assign_target, 'id')) })
        else:
            error(f'[line {definition.lineno}]', 'Core file is not well-formed: only functions, classes, and assignments are allowed on top-level.')
            pp_ast(definition)
            exit(0)

    # now, that we have all the definitions names,
    # we can detect which intrafile dependencies these definitions have
    # (some of these dependencies reference others,
    # and we need to build a graph using Definition objects)
    
    core_library_data = CoreLibrary(core_ast, [], [], [])
    
    for definition in top_level_definitions:
        target_ast = definition
        target_definitions_list = []
        
        if isinstance(definition, FunctionDef):
            target_definitions_list = core_library_data.functions 
        elif isinstance(definition, ClassDef):
            target_definitions_list = core_library_data.classes
        elif isinstance(definition, Assign):
            target_ast = definition.value
            target_definitions_list = core_library_data.variables
                
        dependencies_set = find_known_dependencies_in_ast(target_ast, { **detected_functions, **detected_classes, **detected_variables })

        if isinstance(definition, FunctionDef) or isinstance(definition, ClassDef):
            target_definitions_list.append(Definition(definition.name, list(dependencies_set)))
        elif isinstance(definition, Assign):
            # TODO: REWRITE FOR CORRECT MULTIASSIGNMENT HANDLING
            for assign_target in definition.targets:
                target_definitions_list.append(Definition(getattr(assign_target, 'id'), list(dependencies_set)))
                
    return core_library_data
        

def load_core(path_to_core_file: str):
    core_tree = load_core_ast(path_to_core_file)
    core_library = parse_core(core_tree)
    
    return core_library